using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Nrml_NotationClass
    {
        public static string NrmlNotation(string s)
        {
            Stack<string> tks = new Stack<string>(s.Split(','));
            
            string x, y, op; double w, z; string exception = "Invalid Input"; 

            x = tks.Pop();
            op = tks.Pop();
            y = tks.Pop();
            try { w = Convert.ToDouble(x); z = Convert.ToDouble(y); }
            catch { return exception; }
           
            if (op == "+")
            { z += w; }
            else if (op == "-")
            { z -= w; }
            else if (op == "*")
            { z *= w; }
            else if (op == "/")
            { z /= w; }
           
            return Convert.ToString(z);
        }   
    }
}

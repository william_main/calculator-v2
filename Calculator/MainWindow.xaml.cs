using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      

        string TextboxxNum;
        string firstNum, secondNum;
        string opFunction = "=";
        string s = "";
        bool RPN_on = true; 

        public MainWindow()
        {
            InitializeComponent();
            TextBox.Text = "0";       
        }

        //Connects Number Buttons with Textbox.

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn1.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn2.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn3.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn4.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn5.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn6.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn7.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn8.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn9.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
             if (TextBox.Text == "0" || TextBox.Text == "NaN") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(Btn0.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        private void DotBtn_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "0") { TextBox.Text = ""; }
            TextboxxNum = Convert.ToString(DotBtn.Content);
            TextBox.Text = TextBox.Text + TextboxxNum;
        }

        //Operation

        private void PlusBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                firstNum = TextBox.Text;
                TextBox.Text = "0";
                opFunction = "+";
            }
            catch
            {
                TextBox.Text = "0";
                firstNum = "0";
                secondNum = "0";
            }
        }
        private void MinusBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                firstNum = TextBox.Text;
                TextBox.Text = "0";
                opFunction = "-";
            }
            catch
            {
                TextBox.Text = "0";
                firstNum = "0";
                secondNum = "0";
            }
        }

        private void MultiplyBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                firstNum = TextBox.Text;
                TextBox.Text = "0";
                opFunction = "*";
            }
            catch
            {
                TextBox.Text = "0";
                firstNum = "0";
                secondNum = "0";
            }
        }

        private void DevideBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                firstNum = TextBox.Text;
                TextBox.Text = "0";
                opFunction = "/";
            }
            catch
            {
                TextBox.Text = "0";
                firstNum = "0";
                secondNum = "0";
            }
        }


        //Equals

        private void EqualsBtn_Click(object sender, RoutedEventArgs e)
        {
            secondNum = TextBox.Text;
            if (RPN_on == true)
            {
                s = firstNum + " " + secondNum + " " + opFunction;
                TextBox.Text = Class_RPN.RPN(s);
            }
            else
            {
                s = firstNum + "," + opFunction + "," + secondNum;
                TextBox.Text = Nrml_NotationClass.NrmlNotation(s);
            }
        }


        //Plus Minus
        private void PlsMnsBtn_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text.Contains("-"))
            { TextBox.Text = TextBox.Text.Remove(0, 1); }
            else
            { TextBox.Text = "-" + TextBox.Text; }
        }


        //Delete

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox.Text == "")
            { TextBox.Text = ""; }
            else
            { TextBox.Text = TextBox.Text.Remove(TextBox.Text.Length - 1, 1); }
        }

        //Clear

        private void ClearAllBtn_Click(object sender, RoutedEventArgs e)
        {
            TextBox.Text = "0";
            firstNum = "0";
            secondNum = "0";
        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        { TextBox.Text = ""; }

        //other
        private void Change_Button_Click(object sender, RoutedEventArgs e)
        {
            if (RPN_on == false)
            {
                RPN_on = true;
                Change_Button.Content = "RPN";
            }
            else
            {
                RPN_on = false;
                Change_Button.Content = "standard notation";
            }
        }
    }
}
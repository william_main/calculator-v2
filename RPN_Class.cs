﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    class RPN_Class
    {
        public static string RPN(string s)
        {
            char[] sp = new char[] { ' ', '\t' };
            string answer;
            if (s == null)
            {
                answer = "";
            }
            Stack<string> tks = new Stack<string>
                (s.Split(sp, StringSplitOptions.RemoveEmptyEntries));
            if (tks.Count == 0)
            { }
            try
            {
                string r = Convert.ToString(evalrpn(tks));
                if (tks.Count != 0) throw new Exception();
                return r;
            }
            catch (Exception)
            {
                answer = "Invalid Input";
                return answer;
            }
        }
        private static double evalrpn(Stack<string> tks)
        {
            string tk = tks.Pop();
            double x, y;

            if (!Double.TryParse(tk, out x))
            {
                y = evalrpn(tks); x = evalrpn(tks);

                if (tk == "+") x += y;
                else if (tk == "-") x -= y;
                else if (tk == "*") x *= y;
                else if (tk == "/") x /= y;
                else throw new Exception();
            }
            return x;
        }
    }
}